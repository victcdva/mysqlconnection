package MySqlCrud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlConnection {
    private static final String _url = "jdbc:mysql://localhost:3306/places?&useSSL=false";    
    private static final String _driverName = "com.mysql.jdbc.Driver";   
    private static final String _username = "root";   
    private static final String _password = "root";
    private static Connection _connection;
    
    public Connection Connection() {
        try {
            Class.forName(_driverName);
            try {                
                _connection = DriverManager.getConnection(_url, _username, _password);                
            } catch (SQLException ex) {                
                System.out.println("Failed to create the database connection. " + ex.getMessage());                 
            }
        } catch (ClassNotFoundException ex) {            
            System.out.println("Driver not found.");             
        }
        return _connection;
    }
    
}
