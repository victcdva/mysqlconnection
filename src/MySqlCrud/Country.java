package MySqlCrud;
import java.sql.*;
import MySqlCrud.MySqlConnection;
import java.util.ArrayList;
import java.util.List;

public class Country {

    private String _id;
    private String _name;
    
    public String getId() { return _id; }
    public void setId(String _id) { this._id = _id; }
    public String getName() { return _name; }
    public void setName(String _name) { this._name = _name; }
    
    public Country() {
        this._id = "";
        this._name = "";
    }
    
    public Country(String id, String name) {
        this._id = id;
        this._name = name;
    }    

    public void add(String id, String name) {
        MySqlConnection mc = new MySqlConnection();
        Connection connection = mc.Connection();
        try {
            String query = "insert into countries(id, name) values(?, ?)";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, id);
            statement.setString(2, name);
            //System.out.print(statement);
            statement.executeUpdate();
            System.out.println("Country was added successfully!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void addByProcedure(String id, String name) {
        MySqlConnection mc = new MySqlConnection();
        Connection connection = mc.Connection();
        try {
            String query = "{call AddCountry(?, ?)}";
            CallableStatement statement = connection.prepareCall(query);
            statement.setString(1, id);
            statement.setString(2, name);
            //System.out.print(statement);
            statement.executeUpdate();
            System.out.println("Country was added by procedure successfully!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void getAll() {
        MySqlConnection mc = new MySqlConnection();
        Connection connection = mc.Connection();
        try {
            String query = "select id, name from countries order by name;";
            PreparedStatement statement = connection.prepareStatement(query);
            //System.out.print(statement);
            ResultSet result = statement.executeQuery(query);
            while(result.next()) {
                    String id = result.getString("id");
                    String name = result.getString("name");
                    System.out.println(id + " " + name);
                }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
    }
    
    public void getAll(String id) {
        MySqlConnection mc = new MySqlConnection();
        Connection connection = mc.Connection();
        try {
            String query = "select id, name from countries where id = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, id);
            //System.out.print(statement);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                    id = result.getString("id");
                    String name = result.getString("name");
                    System.out.println(id + " " + name);
                }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void getAllByProcedure(String id) {
        //we instantiate the MySQL class
        MySqlConnection mc = new MySqlConnection();
        /*Connection It is the interface that refers 
           to the connection driver
        */
        Connection connection = mc.Connection();
        try {
            //we assign the query to the "query" variable
            String query = "{call selectCountry(?)}";
            //We prepare the query
            CallableStatement statement = connection.prepareCall(query);
            //We assign value that corresponds to "?"
            statement.setString(1, id);
            //System.out.print(statement);
            //we execute the query
            ResultSet result = statement.executeQuery();
            /*While the result brings me data, 
                show me the corresponding data 
                with its respective field on the console*/
            while(result.next()) {
                    id = result.getString("id");
                    String name = result.getString("name");
                    System.out.println(id + " " + name);
                }            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void update(String id, String idnew, String name){
        MySqlConnection mc = new MySqlConnection();
        Connection connection = mc.Connection();        
        try {
            String query = "update countries set id = ?, name = ? where id = ?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, idnew);
            statement.setString(2, name);
            statement.setString(3, id);
            //System.out.print(statement);
            statement.executeUpdate();
            System.out.println("Country was updated successfully!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }   
    }
    
    public void updateByProcedure(String id, String idnew, String name) {
        MySqlConnection mc = new MySqlConnection();
        Connection connection = mc.Connection();
        try {
            String query = "{call updateCountry(?, ?, ?)}";
            CallableStatement statement = connection.prepareCall(query);
            statement.setString(1, idnew);
            statement.setString(2, name);
            statement.setString(3, id);
            //System.out.print(statement);
            statement.executeUpdate();
            System.out.println("Country was updated by procedure successfully!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void delete(String id){
        MySqlConnection mc = new MySqlConnection();
        Connection connection = mc.Connection();
        try {
            String query = "delete from countries where id = ?";
            PreparedStatement statement = connection.prepareStatement(query);            
            statement.setString(1, id);
            //System.out.print(statement);
            statement.executeUpdate();
            System.out.println("Country was deleted successfully!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
    public void deleteByProcedure(String id) {
        MySqlConnection mc = new MySqlConnection();
        Connection connection = mc.Connection();
        try {
            String query = "{call deleteCountry(?)}";
            CallableStatement statement = connection.prepareCall(query);
            statement.setString(1, id);
            //System.out.print(statement);
            statement.executeUpdate();
            System.out.println("Country was deleted by procedure successfully!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
