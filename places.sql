create database places;
use places;

CREATE TABLE `countries` (
  `id` varchar(5) COLLATE utf8_spanish_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);
COMMIT;

INSERT INTO `countries` (`id`, `name`) VALUES
('CAN', 'Canada'), ('ENG', 'England'),
('FRA', 'France'), ('GER', 'Germany'),
('ITA', 'Italy'), ('MEX', 'Mexico'),
('USA', 'United States');

select * from countries;

-- add procedure
DELIMITER //
CREATE PROCEDURE AddCountry(c_id varchar(5), c_name varchar(50))
BEGIN
    insert into countries(id, name) values(c_id, c_name);
END //
DELIMITER ;

-- read procedure
DELIMITER //
CREATE PROCEDURE selectCountry(c_id varchar(5))
BEGIN
    select id, name from countries where id = c_id;
END //
DELIMITER ;

-- update procedure
DELIMITER //
CREATE PROCEDURE updateCountry(c_id varchar(5), c_idnew varchar(5), c_name varchar(50))
BEGIN
    update countries set id = c_idnew, name = c_name where id = c_id;
END //
DELIMITER ;

-- delete procedure
DELIMITER //
CREATE PROCEDURE deleteCountry(c_id varchar(5))
BEGIN
    delete from places.countries where id = c_id;
END //
DELIMITER ;

call selectCountry("a");
call AddCountry("FFF", "FAAAAA");
call deleteCountry("FFF");
call updateCountry("AAA", "a", "t");
